{
  description = "C3D2 Tox bot";

  edition = 201909;

  inputs.nimble.uri = "github:nix-community/flake-nimble";

  outputs = { self, nixpkgs, nimble }:
    let
      localSystems = [ "x86_64-linux" ];
      crossSystems = [ "x86_64-genode" ];

      forAllLocalSystems = f:
        nixpkgs.lib.genAttrs localSystems (system:
          f {
            inherit system;
            localSystem = system;
            crossSystem = system;
          });

      forAllCrossSystems = f:
        with builtins;
        let
          f' = localSystem: crossSystem:
            let system = localSystem + "-" + crossSystem;
            in {
              name = system;
              value = f { inherit system localSystem crossSystem; };
            };
          list = nixpkgs.lib.lists.crossLists f' [ localSystems crossSystems ];
        in listToAttrs list;

      forAllSystems = f: (forAllLocalSystems f) // (forAllCrossSystems f);

      buildBot = { system, localSystem, crossSystem }:

        nimble.defaultPackage.${system}.buildNimble {
          name = "hqtoxbot";
          src = self;
          homepage = "https://gitea.c3d2.de/ehmry/hqtoxbot";
        };

    in {

      defaultPackage = forAllSystems buildBot;

      packages = let
        f = { system, localSystem, crossSystem }: {
          hqtoxbot = builtins.getAttr system self.defaultPackage;
        };
      in forAllSystems f;
    };
}
