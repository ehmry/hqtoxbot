# Package

version       = "0.1.0"
author        = "Emery Hemingway"
description   = "A Tox bot for C3D2 HQ"
license       = "GPL-3.0-or-later"
srcDir        = "src"
bin           = @["hqtoxbot"]

# Dependencies

requires "nim >= 1.0.0", "toxcore >= 0.2.0"

import distros
if detectOs(NixOS):
  foreignDep "openssl"
