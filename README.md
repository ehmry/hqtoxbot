# HQ Tox Bot

A [counter-anti-disintermediation](https://wiki.p2pfoundation.net/Counter-Anti-Disintermediation) 
hackspace status bot, capable of operating by local network discovery alone.

## Features
 * HQ status updates
 * Locks the door
 * Unlocks the door
 * Opportunity for someone to write a proper Tox bot framework for Nim
 * Tox group invites
 * The cops can't do shit about it

## Authorization

The initial user is hardcoded and addition users are added via the `invite` 
command. Send me (@ehmry) your Tox ID via XMPP or IRL.
